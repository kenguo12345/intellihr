<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #factory(App\User::class, 50)->create();

        \App\User::create([
            'name' => 'user',
            'email' => 'm.basra@live.com',
            'password' => bcrypt('secret'),
            'is_admin' => false,
            'remember_token' => Str::random(10),
        ]);
        \App\User::create([
            'name' => 'user2',
            'email' => 'test@live.com',
            'password' => bcrypt('secret'),
            'is_admin' => false,
            'remember_token' => Str::random(10),
        ]);
        \App\User::create([
            'name' => 'Ken guo',
            'email' => 'ken.guo12345@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => true,
            'remember_token' => Str::random(10),
        ]);
        \App\User::create([
            'name' => 'kimmy',
            'email' => 'kim@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin' => false,
            'remember_token' => Str::random(10),
        ]);
    }
}
