<?php

use Illuminate\Database\Seeder;

class NamesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Name::create([
            'value' => 'jack',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(26),
            'user_id' => 1,
            'used_user_id' => 1,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'Rose',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(25),
            'user_id' => 2,
            'used_user_id' => 2,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'James',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(29),
            'user_id' => null,
            'used_user_id' => 2,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'Kim',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(29),
            'user_id' => 3,
            'used_user_id' => 3,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'Kimmy',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(29),
            'user_id' => null,
            'used_user_id' => 2,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'Jimmy',
            'created_at' => \Carbon\Carbon::now()->subDays(130),
            'updated_at' => \Carbon\Carbon::now()->subDays(29),
            'user_id' => null,
            'used_user_id' => 2,
            'pending' => false
        ]);
        \App\Name::create([
            'value' => 'Timmy',
            'created_at' => \Carbon\Carbon::now()->subDays(360),
            'updated_at' => \Carbon\Carbon::now()->subDays(360),
            'user_id' => 4,
            'used_user_id' => 2,
            'pending' => false
        ]);


    }
}
