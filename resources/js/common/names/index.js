// libs
import { connect } from "react-redux";
import Name from "../../modules/name/Name";

// components
import Names from "./components/Names";

const mapStateToProps = (state) => {
  const { data, ...meta } = state.names;

  return {
    names: data.map((name) => new Name(name)),
    meta: Object.assign({}, meta),
  };
};

export default connect(mapStateToProps)(Names);
