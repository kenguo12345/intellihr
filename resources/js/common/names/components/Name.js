// import libs
import React from "react";
import PropTypes from "prop-types";

// import components
import { Link } from "react-router-dom";

const displayName = "NameComponent";
const propTypes = {
  index: PropTypes.number.isRequired,
  name: PropTypes.object.isRequired,
};

function render({ name }) {
  return <p>{name.value}</p>;
}

render.displayName = displayName;
render.propTypes = propTypes;

export default render;
