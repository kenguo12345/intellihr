// import modular routes
import webRoutes from "../modules/web/routes";
import authRoutes from "../modules/auth/routes";
import nameRoutes from "../modules/name/routes";

export default [...webRoutes, ...authRoutes, ...nameRoutes];
