// import lib
import { lazy } from "react";

export default [
  {
    path: "/names",
    exact: true,
    auth: true,
    component: lazy(() => import("./pages/list/index")),
  },
  {
    path: "/names/pastnames/:id",
    exact: true,
    auth: true,
    component: lazy(() => import("./pages/offical/pastNames/index")),
  },
  {
    path: "/names/citizen",
    exact: true,
    auth: true,
    component: lazy(() => import("./pages/citizen/index")),
  },
  {
    path: "/names/offical",
    exact: true,
    auth: true,
    component: lazy(() => import("./pages/offical/index")),
  },
];
