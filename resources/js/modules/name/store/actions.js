import { NAME_ADD, NAME_UPDATE, NAME_REMOVE, NAME_LIST } from "./action-types";

export function add(payload) {
  return {
    type: NAME_ADD,
    payload,
  };
}

export function update(payload) {
  return {
    type: NAME_UPDATE,
    payload,
  };
}

export function remove(payload) {
  return {
    type: NAME_REMOVE,
    payload,
  };
}

export function list(payload) {
  return {
    type: NAME_LIST,
    payload,
  };
}
