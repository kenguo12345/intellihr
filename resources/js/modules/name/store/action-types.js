export const NAME_LIST = "NAME_LIST";
export const NAME_ADD = "NAME_ADD";
export const NAME_UPDATE = "NAME_UPDATE";
export const NAME_REMOVE = "NAME_REMOVE";

export default {
  NAME_LIST,
  NAME_ADD,
  NAME_UPDATE,
  NAME_REMOVE,
};
