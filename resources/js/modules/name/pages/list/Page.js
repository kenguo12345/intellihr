// import libs
import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {
  nameListRequest,
  nameUpdateRequest,
  nameRemoveRequest,
} from "../../service";

// import components
import NameRow from "./components/NameRow";
import Pagination from "./components/Pagination";
import { Link } from "react-router-dom";

class Page extends Component {
  static displayName = "NamesPage";
  static propTypes = {
    meta: PropTypes.object.isRequired,
    names: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("ken");
    const { dispatch } = this.props;

    dispatch(nameListRequest({}));
  }

  pageChange = (pageNumber) => {
    this.props.dispatch(nameListRequest({ pageNumber }));
  };

  togglePublish = (id) => {
    const name = this.props.names.find((name) => name.id === id);

    if (!name) return;

    name.published = !name.published;
    if (name.published) {
      name.publishedAt = moment();
    } else {
      name.publishedAt = null;
    }

    this.props.dispatch(nameUpdateRequest(name.toJson()));
  };

  handleRemove = (id) => {
    this.props.dispatch(nameRemoveRequest(id));
  };

  renderNames() {
    return this.props.names.map((name, index) => {
      return (
        <NameRow
          key={index}
          name={name}
          index={index}
          togglePublish={this.togglePublish}
          handleRemove={this.handleRemove}
        />
      );
    });
  }

  render() {
    return (
      <main className="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
        <h1>names</h1>
        <table className="table table-responsive table-striped">
          <thead className="thead-inverse">
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Description</th>
              <th>Created At</th>
              <th>Updated At</th>
              <th>Published At</th>
              <th>
                <Link to="/articles/create" className="btn btn-success">
                  Add
                </Link>
              </th>
            </tr>
          </thead>
          <tbody>{this.renderNames()}</tbody>
        </table>
        <Pagination meta={this.props.meta} onChange={this.pageChange} />
      </main>
    );
  }
}

export default Page;
