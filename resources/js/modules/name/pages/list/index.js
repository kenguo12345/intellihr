// import libs
import { connect } from "react-redux";
import Name from "../../Name";

// import components
import Page from "./Page";

const mapStateToProps = (state) => {
  const { data, ...meta } = state.names;

  return {
    names: data.map((name) => new Name(name)),
    meta: Object.assign({}, meta),
  };
};

export default connect(mapStateToProps)(Page);
