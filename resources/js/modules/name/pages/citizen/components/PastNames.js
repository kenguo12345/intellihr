import React, { Component } from "react";
import PropTypes from "prop-types";
import Name from "./Name";

class Names extends Component {
  static displayName = "Names";
  static propTypes = {
    names: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.state = {
      //
    };
  }

  renderNames() {
    return this.props.names.map((name, index) => {
      return <Name key={`name-${index}`} index={index} name={name} />;
    });
  }

  render() {
    return (
      <section id="components-pastnames">
        <div className="container">
          <h3>Past Names</h3>
          <div className="row">{this.props.names && this.renderNames()}</div>
        </div>
      </section>
    );
  }
}

export default Names;
