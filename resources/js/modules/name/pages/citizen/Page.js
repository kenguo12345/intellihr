// import libs
import React, { useEffect, useState } from "react";

import Http from "../../../../utils/Http";

const Page = () => {
  const [pastNames, setPastNames] = useState([]);
  const [currentName, setCurrentName] = useState();
  const [error, setError] = useState();
  const [nameValue, setNameValue] = useState("");
  const [load, setLoad] = useState(true);
  useEffect(() => {
    getPastName();
    getCurrentName();
  }, []);

  const getPastName = () => {
    Http.get("names/pastnames")
      .then((res) => {
        setPastNames(res.data.data);
        setLoad(false);
      })
      .catch((err) => {
        console.log(err);
        setLoad(false);
      });
  };
  const getCurrentName = () => {
    Http.get("names/currentname")
      .then((res) => {
        setCurrentName(res.data.value);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleChange = (name, value) => {
    setNameValue(value);
  };
  const handleChangeSubmit = () => {
    let submitData = { name: nameValue };
    Http.post("names/changename", submitData)
      .then((res) => {
        let tempName = { value: currentName };
        setPastNames((pastNames) => [...pastNames, tempName]);
        setCurrentName(nameValue);
        setError();
      })
      .catch((err) => {
        console.log(err);
        setError("name exist or is being used");
      });
  };

  const CurrentName = () => {
    return <span>{currentName}</span>;
  };

  const PastNames = () => {
    if (load) {
      return <p>loading...</p>;
    } else {
      return pastNames.map((name, i) => {
        return (
          <div key={i}>
            <li key={i}>{name.value}</li>
          </div>
        );
      });
    }
  };

  return (
    <main className="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
      <h1>
        Current name: <CurrentName />
      </h1>
      <table className="table table-responsive table-striped">
        <thead className="thead-inverse">
          <tr>
            <th>Past Names</th>
          </tr>
        </thead>
      </table>
      <div>
        <PastNames />
      </div>
      <br />
      <input
        type="text"
        id="nameValue"
        name="nameValue"
        placeholder=""
        value={nameValue || ""}
        onChange={(e) => handleChange(e.target.name, e.target.value)}
      />

      <button onClick={(e) => handleChangeSubmit(e)}>Change Name</button>
      <p>{error}</p>
    </main>
  );
};

export default Page;
