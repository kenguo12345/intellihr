import { connect } from "react-redux";

// import components
import Page from "./Page";

const mapStateToProps = (state, router) => {
  //console.log(state);
  //console.log(router);
  const { params } = router.match;
  return {
    id: params.id,
  };
};

export default connect(mapStateToProps)(Page);
