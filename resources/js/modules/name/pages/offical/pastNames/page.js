// import libs
import React, { useEffect, useState } from "react";

import Http from "../../../../../utils/Http";

const Page = (props) => {
  const { id } = props;
  const [pastNames, setPastNames] = useState([]);
  const [load, setLoad] = useState(true);
  useEffect(() => {
    getPastName();
  }, []);

  const getPastName = () => {
    Http.get(`names/pastnames/${id}`)
      .then((res) => {
        console.log(res.data);
        setPastNames(res.data.data);
        setLoad(false);
      })
      .catch((err) => {
        console.log(err);
        setLoad(false);
      });
  };

  const PastNames = () => {
    if (load) {
      return (
        <tr key="1">
          <th scope="row">Loading...</th>
        </tr>
      );
    } else {
      return pastNames.map((name, i) => {
        return (
          <tr key={i}>
            <th scope="row">{i + 1}</th>
            <td>{name.value}</td>
            <td>{name.updated_at}</td>
          </tr>
        );
      });
    }
  };

  return (
    <main className="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
      {/* <h1>
        Current name: <CurrentName />
      </h1> */}
      <table className="table table-responsive table-striped">
        <thead className="thead-inverse">
          <tr>
            <th>#</th>
            <th>Past Name</th>
            <th>Updated_At</th>
          </tr>
        </thead>
        <tbody>
          <PastNames />
        </tbody>
      </table>
    </main>
  );
};

export default Page;
