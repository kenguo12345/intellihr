// import libs
import React, { useEffect, useState } from "react";

import Http from "../../../../utils/Http";

const Page = (props) => {
  const [pastNames, setPastNames] = useState([]);
  const [id, setId] = useState();
  const [error, setError] = useState();
  const [nameValue, setNameValue] = useState("");
  const [load, setLoad] = useState(true);
  const [reload, setReload] = useState(0);

  useEffect(() => {
    getAcitveName();
  }, [reload]);

  const getAcitveName = () => {
    Http.get("names/")
      .then((res) => {
        setPastNames(res.data.data);
        setLoad(false);
      })
      .catch((err) => {
        console.log(err);
        setLoad(false);
      });
  };

  const handleChange = (name, value) => {
    setNameValue(value);
  };
  const handleIdChange = (name, value) => {
    setId(value);
  };
  const handleChangeSubmit = () => {
    let submitData = { name: nameValue };
    Http.post(`names/changename/${id}`, submitData)
      .then((res) => {
        setReload(reload + 1);
        setError();
      })
      .catch((err) => {
        console.log(err);
        setError("Something wrong...");
      });
  };

  const ActiveNames = () => {
    if (load) {
      return (
        <tr key="1">
          <th scope="row">Loading...</th>
        </tr>
      );
    } else {
      return pastNames.map((name, i) => {
        return (
          <tr key={i}>
            <th scope="row">{i + 1}</th>
            <td>{name.user_id}</td>
            <td>{name.value}</td>
          </tr>
        );
      });
    }
  };

  return (
    <main className="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
      <table className="table table-responsive table-striped">
        <thead className="thead-inverse">
          <tr>
            <th>#</th>
            <th>User ID</th>
            <th>Active Name</th>
          </tr>
        </thead>
        <tbody>
          <ActiveNames />
        </tbody>
      </table>

      <br />
      <p>User ID: </p>
      <input
        type="text"
        id="idValue"
        name="idValue"
        onChange={(e) => handleIdChange(e.target.name, e.target.value)}
      />
      <p>New Name: </p>
      <input
        type="text"
        id="nameValue"
        name="nameValue"
        onChange={(e) => handleChange(e.target.name, e.target.value)}
      />
      <br />

      <button onClick={(e) => handleChangeSubmit(e)}>Change Name</button>
      <p>{error}</p>
    </main>
  );
};

export default Page;
