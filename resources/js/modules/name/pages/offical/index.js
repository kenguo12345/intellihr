// import libs
import { connect } from "react-redux";
import User from "../../../user/User";

// import components
import Page from "./Page";

const mapStateToProps = (state) => {
  const { data, ...meta } = state.names;

  return {
    meta: Object.assign({}, meta),
    user: new User(state.user),
  };
};

export default connect(mapStateToProps)(Page);
