import Http from "../../utils/Http";
import Transformer from "../../utils/Transformer";
import * as nameActions from "./store/actions";

function transformRequest(parms) {
  return Transformer.send(parms);
}

function transformResponse(params) {
  return Transformer.fetch(params);
}

export function nameAddRequest(params) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      Http.post("/names", transformRequest(params))
        .then((res) => {
          dispatch(nameActions.add(transformResponse(res.data)));
          return resolve();
        })
        .catch((err) => {
          const statusCode = err.response.status;
          const data = {
            error: null,
            statusCode,
          };

          if (statusCode === 422) {
            const resetErrors = {
              errors: err.response.data,
              replace: false,
              searchStr: "",
              replaceStr: "",
            };
            data.error = Transformer.resetValidationFields(resetErrors);
          } else if (statusCode === 401) {
            data.error = err.response.data.message;
          }
          return reject(data);
        });
    });
}

export function nameUpdateRequest(params) {
  return (dispatch) =>
    new Promise((resolve, reject) => {
      Http.patch(`names/${params.id}`, transformRequest(params))
        .then((res) => {
          dispatch(nameActions.add(transformResponse(res.data)));
          return resolve();
        })
        .catch((err) => {
          const statusCode = err.response.status;
          const data = {
            error: null,
            statusCode,
          };

          if (statusCode === 422) {
            const resetErrors = {
              errors: err.response.data,
              replace: false,
              searchStr: "",
              replaceStr: "",
            };
            data.error = Transformer.resetValidationFields(resetErrors);
          } else if (statusCode === 401) {
            data.error = err.response.data.message;
          }
          return reject(data);
        });
    });
}

export function nameRemoveRequest(id) {
  return (dispatch) => {
    Http.delete(`names/${id}`)
      .then(() => {
        dispatch(nameActions.remove(id));
      })
      .catch((err) => {
        // TODO: handle err
        console.error(err.response);
      });
  };
}

export function nameListRequest(params) {
  let { pageNumber = 1, url = "/names/expiring" } = params;
  console.log("namerequest");

  return (dispatch) => {
    if (pageNumber > 1) {
      url = url + `?page=${pageNumber}`;
    }

    Http.get(url)
      .then((res) => {
        console.log("name get hit");
        dispatch(nameActions.list(transformResponse(res.data)));
      })
      .catch((err) => {
        // TODO: handle err
        console.error(err.response);
      });
  };
}
export function pastNameListRequest(id) {
  return (dispatch) => {
    if (pageNumber > 1) {
      url = url + `?page=${pageNumber}`;
    }

    Http.get(url)
      .then((res) => {
        console.log("name get hit");
        dispatch(nameActions.list(transformResponse(res.data)));
      })
      .catch((err) => {
        // TODO: handle err
        console.error(err.response);
      });
  };
}

export function nameEditRequest(id) {
  return (dispatch) => {
    Http.get(`names/${id}`)
      .then((res) => {
        dispatch(nameActions.add(transformResponse(res.data)));
      })
      .catch((err) => {
        // TODO: handle err
        console.error(err.response);
      });
  };
}

export function nameFetchRequest(slug) {
  return (dispatch) => {
    Http.get(`names/published/${slug}`)
      .then((res) => {
        dispatch(nameActions.add(transformResponse(res.data)));
      })
      .catch((err) => {
        // TODO: handle err
        console.error(err.response);
      });
  };
}
