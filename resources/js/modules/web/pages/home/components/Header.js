import React from "react";

export default function Header() {
  return (
    <header className="bg-primary text-white">
      <div className="container text-center">
        <h1>Ken Guo</h1>
        <p className="lead">Bachleor in Information Technology</p>
        <p className="lead">Fullstack Developer</p>
        <p className="lead">
          <i className="fa fa-heart text-danger" />
          {`{ PHP, JavaScript, Python, MySQL, MongoDB }`}
        </p>
      </div>
    </header>
  );
}

Header.displayName = "HomePageHeader";
