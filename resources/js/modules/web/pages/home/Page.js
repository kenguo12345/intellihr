import React, { useLayoutEffect } from "react";
import PropTypes from "prop-types";

// import components
import Names from "../../../../common/names/index";

// import services
import { nameListRequest } from "../../../name/service";

export default function Page({ dispatch }) {
  useLayoutEffect(() => {
    dispatch(nameListRequest({ url: "/names/expiring" }));
  }, []);

  return (
    <div>
      <Names />
    </div>
  );
}

Page.displayName = "HomePage";
Page.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
