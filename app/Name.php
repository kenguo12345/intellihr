<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class Name extends Model
{
    // use soft delete instead of permanent delete
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'names';


    protected $fillable = ['updated_at', 'used_user_id', 'pending'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'pending' => 'boolean',
    ];




    /**
     * Load all for admin and paginate
     *
     * @return Paginator
     */
    public static function loadAll(): Paginator
    {
        return static::latest()
            ->activeNames()
            ->paginate();
    }


    /**
     * Load all for expiring names and paginate
     *
     * @return Paginator
     */
    public static function loadAllExpiring(): Paginator
    {
        return static::latest()
            ->expiringNames()
            ->activeNames()
            ->paginate();
    }

 

    /**
     * Load all used names and paginate
     *
     * @return Paginator
     */
    public static function loadAllUsedNames(): Paginator
    {
        return static::latest()
            ->inactiveNames()
            ->paginate();
    }

    /**
     * Load all past names and paginate
     *
     * @param $user_id
     * 
     * @return Paginator
     */
    public static function loadAllPastNames(int $user_id): Paginator
    {
        return static::latest()
            ->pastNames($user_id)
            ->inactiveNames()
            ->paginate();
            
    }

    /**
     * Load all for logged in user and paginate
     *
     * @param $user_id
     *
     * @return Paginator
     */
    public static function loadAllMine(int $user_id): Paginator
    {
        return static::latest()
            ->mine($user_id)
            ->paginate();
    }





    /**
     * Add query scope to get all names expires in 28 days
     *
     * @param Builder $query
     *
     * @return Builder
     */
    public function scopeExpiringNames(Builder $query): Builder
    {
        return $query->where(
            'updated_at', '<', \Carbon\Carbon::now()->subDays(365-28)
        );
    }





    /**
     * Load only names related with the user id
     *
     * @param Builder $query
     * @param int $user_id
     *
     * @return Builder
     */
    public function scopeMine(Builder $query, int $user_id): Builder
    {
        return $query->where('user_id', $user_id);
    }

    /**
     * Load only names with not null user id
     *
     * @param Builder $query
     * @param int $user_id
     *
     * @return Builder
     */
    public function scopeActiveNames(Builder $query): Builder
    {
        return $query->whereNotNull('user_id');
    }

    /**
     * Load names with null user id
     *
     * @param Builder $query
     * @param int $user_id
     *
     * @return Builder
     */
    public function scopeInactiveNames(Builder $query): Builder
    {
        return $query->whereNull('user_id');
    }

    /**
     * Load names with null user id and exsiting used_user_id
     *
     * @param Builder $query
     * @param int $user_id
     *
     * @return Builder
     */
    public function scopePastNames(Builder $query, int $user_id): Builder
    {
        return $query->where('used_user_id', $user_id);
    }

    /**
     * Relationship between articles and user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
